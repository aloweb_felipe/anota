@extends('template.master')

@section('conteudo')

    <style>
        .imagem_avatar{
            max-width: 100%;
            padding: 10px
        }
    </style>


    <div class="" style="background-color: #f9dcaf; padding: 15px; border-radius: 5px; margin-right: 45px">
        <div style="background-color: #fdf0dc; border-radius: 5px;">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('aranha')}}"><img src="/img/avatar/aranha.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('batman')}}"><img src="/img/avatar/batman.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('capitao')}}"><img src="/img/avatar/capitao.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('ciborgue')}}"><img src="/img/avatar/ciborgue.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('deadpool')}}"><img src="/img/avatar/deadpool.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('estranho')}}"><img src="/img/avatar/estranho.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('fenix')}}"><img src="/img/avatar/fenix.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('ferro')}}"><img src="/img/avatar/ferro.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('flash')}}"><img src="/img/avatar/flash.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('hulk')}}"><img src="/img/avatar/hulk.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('lanterna')}}"><img src="/img/avatar/lanterna.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('mulher')}}"><img src="/img/avatar/mulher.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('storm')}}"><img src="/img/avatar/storm.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('superman')}}"><img src="/img/avatar/superman.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('thor')}}"><img src="/img/avatar/thor.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('wanda')}}"><img src="/img/avatar/wanda.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href="{{route('wolverine')}}"><img src="/img/avatar/wolverine.png" alt="" class="imagem_avatar"></a></div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a href=""><img src="/img/avatar/escolher.png" alt="" class="imagem_avatar"></a></div>
            </div>
        </div>
    </div>

@endsection