@extends('template.master')

@section('conteudo')
    <style>
        .imagem_grande{
            padding-bottom: 20px;
        }


        label.labelInput input[type="file"] {
            position: fixed;
            top: -1000px;
        }

        .labelInput {
            border: 1px solid #b4aeb5;
            border-radius: 4px;
            padding: 3px 5px;
            margin: 2px;
            background: #DDD;
            display: inline-block;
        }
        .labelInput:hover {
            background: #CCC;
        }
        .labelInput:active {
            background: #CCF;
        }
        .labelInput :invalid + span {
            color: #0700b5;
        }
        .labelInput :valid + span {
            color: #4A4;
        }

        .azul{
            color: #0700b5;
        }
    </style>

    <div style="background-color: #dcccdc;
        border: 0;
        box-shadow: 2px 2px 3px #5f565f;
text-align: center;
vertical-align: middle; height: 75px; margin-bottom: 15px;">
        <h1 style="color: #3366ff; padding-top: 15px"><strong>Editar Perfil</strong></h1>
    </div>
    <div style="background-color: #dcccdc; border: 0; box-shadow: 2px 2px 3px #5f565f;">
        <div class="container" style="padding-top: 35px; padding-bottom: 150px">
            <div class="col-md-2 col-lg-2 col-sm-4 col-xs-12">
                <img src="/img/user/batman.png" style="width: 110px">
                <br><br>
                <center>
                    <label class="labelInput">
                        <input type="file" required/>
                        <span>Escolher foto</span>
                    </label>
                </center>
            </div>

            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">

                    <div class="col-lg-12">

                    <label class="azul">Nome: </label>
                    <input style="border-radius: 5px" type="text" class="form-control" value="{{Auth::user()->name}}">
                        <br>
                    </div>


                    <div class="col-lg-3">
                        <div class="col-lg-5" style="margin-top: 5px; margin-left: -15px">
                            <label class="azul">Idade: </label>
                        </div>
                        <div class="col-lg-7">
                    <select name="" id="" class="form-control" style="border-radius: 5px">
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                    </select>
                        </div>
                    </div>
                    <div class="col-lg-4" style="margin-top: 5px">
                    <label class="azul" style="margin-left: -5px">Sexo: </label>
                    <input type="radio" checked> <span >Masculino</span>
                    <input type="radio"> <span >Feminino</span>
                    </div>
                    <div class="col-lg-5" style="margin-left: -35px">
                        <div class="col-lg-3" style="margin-top: 5px">
                        <label class="azul">Escola: </label>
                        </div>
                        <div class="col-lg-9">
                    <select name="" id="" class="form-control" style="width: 212px; border-radius: 5px">
                        <option value="Fatec Guara">Fatec Guara</option>
                        <option value="não sei">não sei</option>
                        <option value="não sei">não sei</option>
                        <option value="não sei">não sei</option>
                        <option value="não sei">não sei</option>
                    </select>
                        </div>
                    </div>
            </div>
        </div>

    </div>



@endsection