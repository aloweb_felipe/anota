<style>
    .balao{
        border: 2px solid #33b2ff;
        border-radius: 50px 50px 25px 25px;
        width: 300px;
        height: 130px;
        max-height: 100%;
        position: relative;
        text-align: center;
        float: left;
        padding: 20px;
        background-color: #ffffff;
    }
    .balao:after{
        content: "";
        width: 0;
        height: 0;
        position: absolute;
        border-left: 20px solid transparent;
        border-right: 20px solid transparent;
        border-top: 20px solid #fff;
        bottom: -20px;
        right: 50%;
    }
</style>


<h1>

    <div class="col-lg-3" style="padding-left: 40px; padding-top: 25px; margin-top: 25px">
        <div class='balao'>
            <span style="font-family: rptrt !important; font-size: 24px;color: #4340a0; ">
                @if(!auth::user())
                Olá, eu sou o ANotacão!!!
                @endif
                @if(auth::user())
                    {{$dialogo_anotacao}}
                @endif
            </span>
        </div>
        <div style="float: left; padding: 20px">
            <img src="/img/novo_layout/anotacao.png" alt="">
        </div>
    </div>
    <span style="font-size: 1px">.</span>

</h1>