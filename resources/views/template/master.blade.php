<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    @include('template._css')
</head>
<body class="fixed-left" style="background: linear-gradient(to bottom, #5fbdef, #ffffff);">
<!-- Modal Start -->



<!-- Begin page -->
<div id="wrapper" style="font-family: rptrt !important; background: linear-gradient(to bottom, #5fbdef, #eedcb9); height: 100%">



        @include('template._topo')



       @include('template._anotacao')
        <!-- Start right content -->


        <div class="content-page">

            <!-- ============================================================== -->
            <!-- Start Content here -->
            <!-- ============================================================== -->

            <div class="content" style="height: 100% !important; ;margin-left: 55px; padding-left: 55px; margin-top: -55px; padding-top: 55px;">

                @yield('conteudo')

            </div>
            <!-- ============================================================== -->
            <!-- End content here -->
            <!-- ============================================================== -->


        </div>
    <div class="col-lg-12" style="padding: 0; margin: 0">

        @include('template._footer')
    </div>
    <!-- End right content -->

</div>


@include('template._js')

</body>
</html>