<style>
    .top{
        width: 100% !important;
        height: 45px !important;
        background-color: #0559cf;
        box-shadow: 0 3px 15px 1px #222222;
    }
    .top_user_login{
        background-image: url('/img/novo_layout/fundo_menu.png');
        background-repeat: no-repeat;
        background-size: cover;
    }
    .imagem_acima{
        margin-top: -20px !important;
    }

    .imagem_acima2{
        margin-top: 60px !important;
    }

    .input_login{
        background-color: #fdaf35;
        padding: 3px;
        box-shadow: 0 2px 5px 2px #222222;
    }
    .input-login{
        border: 0;
        background-color: #f9dcaf;
    }

    .borda-triangular-direita{
    }
    .borda-triangular-direita:after{
        content: "";
        display: inline-block;
        vertical-align: middle;
        float: right;
        width: 0;
        height: 0;
        border-top: 17px solid transparent;
        border-bottom: 17px solid transparent;
        border-left: 17px solid #fdaf35;
    }
</style>


<div class="top" style="">
    @if (auth::user())
        <div class="col-lg-3"></div>
        <div class="col-lg-3" style="margin-top: 4px">
            <a href="{{route('aula')}}">
            <div class="col-lg-2">
            <img src="/img/novo_layout/icone_aula.png" alt="">
            </div>
            <div class="col-lg-10 borda-triangular-direita" style="margin-top: 2px">
            <span style="color: #fdaf35; font-size: 24px; ">Aulas</span>
            </div>
            </a>
        </div>
        <div class="col-lg-3" style="margin-top: 4px">
            <a href="{{route('suporte')}}">
            <div class="col-lg-2">
            <img src="/img/novo_layout/icone_suporte.png" alt="">
            </div>
            <div class="col-lg-10 borda-triangular-direita" style="margin-top: 2px">
            <span style="color: #fdaf35; font-size: 24px;  ">Suporte</span>
            </div>
            </a>
        </div>
        <div class="col-lg-3" style="margin-top: 4px">
            <div class="col-lg-2">
            <img src="/img/novo_layout/icone_sair.png" alt="">
            </div>
            <div class="col-lg-10 borda-triangular-direita" style="margin-top: 2px">
            <span style="color: #fdaf35; font-size: 24px;  ">
                <a href="{{route('logout')}}" style="color: #fdaf35;" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sair</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </span>
            </div>
        </div>
    @endif
</div>

@if (auth::user())
    <div class="col-lg-12 top_user_login" style="color: #0559cf">
        <div class="col-lg-2">
            <a href="{{ route('home') }}">
            <img class="imagem_acima" src="/img/novo_layout/logo.png" alt="">
            </a>
        </div>
        <div class="col-lg-10" style="float: right; margin-top: 20px">
            <div style=" max-width: 280px; padding: 25px; float: right">
                <div class="col-lg-6" style="margin-top: -20px">
                    <a href="{{route('avatar')}}" class="rounded-image profile-image" style="width: 100px; ">
                        <img src="{{auth::user()->imagem}}">
                    </a>
                    <button style="background: orange; border: 0; margin-left: 29px; position: absolute; margin-top: -15px">{{Auth::user()->name}}</button>
                </div>
                <div class="col-lg-6">
                    <button style="color: orange; background: #0559cf; border: 0; box-shadow: 0 3px 5px 1px #222222;
padding-right: 20px; padding-left: 20px">Editar</button>
                    <br><br>
                    <a href="{{route('notas')}}">
                    <button style="color: orange; background: #0559cf; border: 0; box-shadow: 0 3px 5px 1px #222222;
padding-right: 20px; padding-left: 20px; background: linear-gradient(to right, #0200b5 , #0559cf);">89/100</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endif

@if (!auth::user())
    <div class="col-lg-12 top_user_login" style="color: #0559cf">
        <div class="col-lg-2">
            <a href="{{ route('home') }}">
                @if (!$errors->has('email'))
                    <img class="imagem_acima" src="/img/novo_layout/logo.png" alt="">
                @else
                    <img class="imagem_acima2" src="/img/novo_layout/logo.png" alt="">
                @endif
            </a>
        </div>
        <div class="col-lg-10" style="float: right;">

            <form style="float: right; max-width: 220px; padding: 25px" class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="input_login">
                    Login:
                    <input  style="margin-left: 7px" id="email" name="email" value="{{ old('email') }}" required
                           autofocus class=" text-input input-login">
                </div>
                <br>
                <div class="input_login" style="margin-top: -8px">
                    Senha:
                    <input style="margin-left: 3px" type="password" required name='password' class=" text-input input-login">
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-6" style="margin-top: -5px">
                        <button style="background-color: #fdaf35; border: 0; box-shadow: 0 2px 10px 2px #222222" type="submit" class="">Entrar</button>
                    </div>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <p style="color: #ffd700; font-size: 18px">{{ $errors->first('email') }}</p>
                    </span>
                @endif
            </form>
        </div>
    </div>
@endif

