@extends('template.master')

@section('conteudo')

    <style>
        .input-form{
            background-color: #f9dcaf; font-size: 22px; color: #0559cf
        }
        .label-form{
            color: #fdaf35; font-size: 26px
        }
    </style>


    <div class="" style="background-color: #f9dcaf; padding: 15px; border-radius: 5px; margin-bottom: 150px">
        <div style="background-color: #fdf0dc; border-radius: 5px; padding: 10px; text-align: center">
                <div class="row">
                    <div class="col-lg-12">
                        <span style="font-size: 32px; color: #0559cf;">Desempenho do aluno: </span>
                    </div>
                    <div class="col-lg-12" style="padding-bottom: 10px">
                        <div class="col-lg-1">
                        <span class="label-form">Aluno:</span>
                        </div>
                        <div class="col-lg-11">
                        <input type="text" value="Paulo Felipe" class="form-control input-form">
                        </div>
                    </div>
                    <div class="col-lg-12" style="padding-bottom: 10px">
                        <div class="col-lg-1">
                            <span class="label-form">Media:</span>
                        </div>
                        <div class="col-lg-4">
                            <input type="text" value="7.50" class="form-control input-form">
                        </div>
                        <div class="col-lg-3">
                            <span class="label-form">Media da sala:</span>
                        </div>
                        <div class="col-lg-4">
                            <input type="text" value="7,00" class="form-control input-form">
                        </div>
                    </div>
                    <div class="col-lg-12" style="padding-bottom: 10px">
                        <div class="col-lg-2">
                            <span class="label-form">Aula atual:</span>
                        </div>
                        <div class="col-lg-3">
                            <input type="text" value="7" class="form-control input-form">
                        </div>
                        <div class="col-lg-3">
                            <span class="label-form">Aulas restante:</span>
                        </div>
                        <div class="col-lg-4">
                            <input type="text" value="8,00" class="form-control input-form">
                        </div>
                    </div>
                </div>
        </div>
    </div>

@endsection