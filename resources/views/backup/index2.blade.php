@extends('template.master')

@section('conteudo')

    <style>
        .correto {
            border: 3px solid green;
        }
    </style>
    <div style="background-color: #dcccdc;
        border: 0;
        box-shadow: 2px 2px 3px #5f565f;
text-align: center;
vertical-align: middle; height: 75px; margin-bottom: 15px;">
        <h1 style="color: #5274cb; padding-top: 15px"><strong>Aula 1</strong></h1>
    </div>
    <div style="background-color: #dcccdc;
        border: 0;
        box-shadow: 2px 2px 3px #5f565f;">
    <div class="container" >
        <div class="row" >
            <div >
                <div class="" id="primeiro_um">
                    <br><br><br><br><br><br><br><br>
                    <center>
                    <h1><strong style="font-size: 60px">Seja bem vindo(a) ao ANota!!!</strong></h1>
                    </center>
                    <br><br><br><br><br>
                </div>
                <div class="" style="display: none" id="pergunta_resposta">
                    <header class="container">
                        <br>
                        <h2 id="selecione_algo" style="display: none; color: red">Selecione alguma opção</h2>
                        <br>
                        <h1><strong>1) Qual opção abaixo mostra a bandeira do Brasil?</strong></h1>
                    </header>
                    <section style="padding-left: 150px">
                        <br>
                        <div class="row">
                            <div class="col-md-6" style="padding-top: 15px">
                                <div class="row">
                                    <div class="col-md-3"><img onclick="alemanha()" src="/img/bandeira/alemanha.png" alt=""
                                                               style="width: 200px;"></div>
                                </div>
                                <div class="row" style="text-align: center; padding-left: 65px">
                                    <div class="col-md-3"><input onclick="alemanha()" type="radio" id="alemanha"></div>
                                </div>
                            </div>
                            <div id="div_brasil" class="col-md-6" style="max-width: 238px; padding-top: 15px">
                                <div class="row">
                                    <div class="col-md-3"><img onclick="brasil()" src="/img/bandeira/brasil.png" alt=""
                                                               style="width: 200px;"></div>
                                </div>
                                <div class="row" style="text-align: center; padding-left: 95px">
                                    <div class="col-md-3"><input onclick="brasil()" type="radio" id="brasil"></div>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-6" style="padding-top: 15px">
                                <div class="row">
                                    <div class="col-md-3"><img onclick="inglaterra()" src="/img/bandeira/inglaterra.jpg" alt=""
                                                               style="width: 200px;"></div>
                                </div>
                                <div class="row" style="text-align: center; padding-left: 65px">
                                    <div class="col-md-3"><input onclick="inglaterra()" type="radio" id="inglaterra"></div>
                                </div>
                            </div>
                            <div class="col-md-6" style="max-width: 238px; padding-top: 15px">
                                <div class="row">
                                    <div class="col-md-3"><img onclick="russia()"  src="/img/bandeira/russia.jpg" alt=""
                                                               style="width: 200px;"></div>
                                </div>
                                <div class="row" style="text-align: center; padding-left: 95px">
                                    <div class="col-md-3"><input onclick="russia()" type="radio" id="russia"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <br>
                <div style="padding-left: 322px">
                    <h1 style="padding-left: 130px; display: none" id="sonic_titulo"><strong>Sonic</strong></h1>
                    <iframe style="display: none;margin-left: -161px;height: 500px" width="100%" id="sonic"
                            src="http://www.1001jogos.pt/lib/game/game.php?d=ZmlsZT1odHRwJTNBJTJGJTJGZ2Nkbi4xMDAxam9nb3MucHQlMkYxMzAuc3dmJmF0dHJpYnV0ZXMlNUJmbGFzaF9wYXJhbSU1RCU1QmFsbG93TmV0d29ya2luZyU1RD1pbnRlcm5hbA,,"
                            class="possibleFlashContainer" scrolling="no" frameborder="0"
                            allowtransparency="true"></iframe>
                </div>
            </div>
        </div>
    </div>
    <center>
        <div style="padding: 10px;" class="container">
            <div style="">
                <button onclick="anterior()"
                        style="float: left; padding: 8px; background-color: #a39ef1; border-radius: 5px; font-size: 20px">
                    <strong>< Anterior</strong></button>
                <button id="proximo_primeiro" onclick="proximo_um()"
                        style="float: right; padding: 8px; background-color: #a39ef1; border-radius: 5px; font-size: 20px;">
                    <strong>Próximo ></strong></button>
                <button id="proximo_depois" onclick="proximo()"
                        style="display: none;float: right; padding: 8px; background-color: #a39ef1; border-radius: 5px; font-size: 20px;">
                    <strong>Próximo ></strong></button>
            </div>
        </div>
    </center>
    </div>
    <script>
        function brasil() {
            $('#div_brasil').addClass('correto');
            check('brasil');
            uncheck("alemanha");
            uncheck("russia");
            uncheck("inglaterra");
        }

        function alemanha() {
            $('#div_brasil').removeClass('correto');
            check('alemanha');
            uncheck("brasil");
            uncheck("russia");
            uncheck("inglaterra");
        }

        function russia() {
            $('#div_brasil').removeClass('correto');
            check('russia');
            uncheck("alemanha");
            uncheck("brasil");
            uncheck("inglaterra");
        }

        function inglaterra() {
            $('#div_brasil').removeClass('correto');
            check('inglaterra');
            uncheck("alemanha");
            uncheck("russia");
            uncheck("brasil");
        }

        function proximo_um() {
            $('#pergunta_resposta').show();
            $('#proximo_depois').show();
            $('#primeiro_um').hide();
            $('#proximo_primeiro').hide();

        }

        function anterior() {
            $('#pergunta_resposta').show();
            $('#sonic').hide();
            $('#sonic_titulo').hide();
        }
        function proximo() {
            $('#pergunta_resposta').hide();
            $('#sonic').show();
            $('#sonic_titulo').show();
        }


        function check(name) {
            document.getElementById(name).checked = true;
        }

        function uncheck(name) {
            document.getElementById(name).checked = false;
        }

    </script>
@endsection