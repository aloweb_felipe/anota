@extends('template.master')

@section('conteudo')




    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <script>
        $(document).ready(function(){
            var banner = $('#mudar_imagem'), x = 1 + Math.floor(Math.random() * 3);
            if (x == 1) {
                banner.css({ 'background-image': 'url(/img/gif/anotacao/1.gif)',
                             'background-size': 'cover'});
            }
            else if (x == 2) {
                banner.css({ 'background-image': 'url(/img/gif/anotacao/2.gif)',
                    'background-size': 'cover'});
            }
            else if (x == 3) {
                banner.css({ 'background-image': 'url(/img/gif/anotacao/3.gif)',
                    'background-size': 'cover'});
            }
        });

        function mudar_imagem() {
            console.log('fui chamado');
            var banner = $('#mudar_imagem'), x = 1 + Math.floor(Math.random() * 3);
            if (x == 1) {
                banner.css({ 'background-image': 'url(/img/gif/anotacao/1.gif)',
                    'background-size': 'cover'});
            }
            else if (x == 2) {
                banner.css({ 'background-image': 'url(/img/gif/anotacao/2.gif)',
                    'background-size': 'cover'});
            }
            else if (x == 3) {
                banner.css({ 'background-image': 'url(/img/gif/anotacao/3.gif)',
                    'background-size': 'cover'});
            }
        }

        setInterval('mudar_imagem()',6500);
        setTimeout('mudar_imagem()', 1000000);
    </script>

    <div class="container" id="mudar_imagem">
    {{--<div class="container" style="background: url('/img/gif/anotacao/Floating_with_ball.gif'); background-size: cover">--}}
        <div class="full-content-center" style="padding-top: 5%; margin-bottom: 180px;">
            <div class="login-wrap animated flipInX" style="box-shadow: 5px 15px 10px rgba(6,13,144,0.4); background-color: rgba(255,255,255, 0.9)">
                    <div class="login-block">
                        <img src="/img/logo/logo.png" class="not-logged-avatar"
                             style="width: 150px; border: inherit; box-shadow: none">
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} login-input">
                                <i class="fa fa-user overlay"></i>
                                <input id="email" type="email" name="email" value="{{ old('email') }}" required
                                       autofocus class="form-control text-input" placeholder="Email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} login-input">
                                <i class="fa fa-key overlay"></i>
                                <input type="password" required name='password' class="form-control text-input"
                                       placeholder="********">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <teste style="color: black">Lembre-me</teste>
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-success btn-block">LOGIN</button>
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{ route('password.request') }}" class="btn btn-default btn-block">Esqueceu
                                        sua senha?</a>
                                </div>
                            </div>
                        </form>
                    </div>
        </div>
        </div>
    </div>

@endsection
