<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    @include('template._css')
</head>
<body class="fixed-left" style="background: url('/img/fundo/background_login.png');">
<!-- Modal Start -->

<!-- Modal Logout -->
<div class="md-modal md-just-me" id="logout-modal">
    <div class="md-content">
        <h3>Confirmação <strong>Logout</strong></h3>
        <div>
            <p class="text-center">Você tem certeza?</p>
            <p class="text-center">
                <button class="btn btn-danger md-close">Não!</button>
                <a href="{{route('logout')}}" class="btn btn-success md-close"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sim, eu tenho</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            </p>
        </div>
    </div>
</div>
<!-- Modal End -->


<!-- Begin page -->
<div id="wrapper" style="font-family: rptrt !important; background: url('/img/fundo/background_login.png');">

@if (auth::user())

    {{--@include('template._topo')--}}

    @include('backup._menuEsquerdo')

@endif
    <!-- Start right content -->
    @if (auth::user())
    <div class="content-page">
    @endif
        <!-- ============================================================== -->
        <!-- Start Content here -->
        <!-- ============================================================== -->

        <div class="content" style="margin-top: -10px; margin-left: 20px">

            @yield('conteudo')
            @if (auth::user())
            @include('template._anotacao')
            @endif

        @if (auth::user())

           @include('template._footer')
        @endif
        </div>
        <!-- ============================================================== -->
        <!-- End content here -->
        <!-- ============================================================== -->

    @if (auth::user())
    </div>
    @endif
    <!-- End right content -->

</div>

<!-- End of page -->
<!-- the overlay modal element -->
<div class="md-overlay"></div>
<!-- End of eoverlay modal -->

@include('template._js')

</body>
</html>