

CREATE TABLE tip_tipo_usuario (
  tip_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  tip_nome VARCHAR(120) NULL,
  tip_descricao TEXT NOT NULL,
  PRIMARY KEY (tip_id))
ENGINE = InnoDB;

CREATE TABLE esc_escola(
  esc_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  esc_login VARCHAR(120),
  esc_senha VARCHAR(20),
  esc_nome VARCHAR(120),
  esc_cnpj VARCHAR(14),
  esc_qnt_alunos INT,
  esc_qnt_professores INT,
  PRIMARY KEY (esc_id))
ENGINE = InnoDB;


CREATE TABLE usu_usuario (
  usu_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  usu_nome VARCHAR(120),
  usu_data_nascimento DATE,
  usu_sexo CHAR (1),
  usu_rg VARCHAR(14),
  usu_cpf VARCHAR(11),
  usu_email VARCHAR(120),
  usu_senha VARCHAR(20),
  usu_token VARCHAR(120),
  tip_id INT,
  esc_id INT,
  usu_visibilidade TINYINT,
  usu_ra INT,
  usu_ultimo_login DATETIME,
  PRIMARY KEY (usu_id),
  INDEX (tip_id ASC),
  INDEX fk_usu_usuario_esc_escola1_idx (esc_id ASC),
  CONSTRAINT
    FOREIGN KEY (tip_id)
    REFERENCES nota.tip_tipo_usuario (tip_id),
  CONSTRAINT fk_usu_usuario_esc_escola1
    FOREIGN KEY (esc_id)
    REFERENCES nota.esc_escola (esc_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE end_endereco (
  end_id INT NOT NULL AUTO_INCREMENT UNIQUE,
  end_rua VARCHAR(120),
  end_numero VARCHAR(6),
  end_bairro VARCHAR(60),
  end_cidade VARCHAR(60),
  end_estado VARCHAR(2),
  end_cep VARCHAR(8),
  usu_id INT,
  PRIMARY KEY (end_id),
  INDEX fk_end_endereco_usu_usuario1_idx (usu_id ASC),
  CONSTRAINT fk_end_endereco_usu_usuario1
    FOREIGN KEY (usu_id)
    REFERENCES nota.usu_usuario (usu_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



CREATE TABLE tur_turma (
  tur_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  tur_nome VARCHAR(15),
  tur_periodo VARCHAR(8),
  usu_usuario_usu_id INT,
  PRIMARY KEY (tur_id),
  INDEX fk_tur_turma_usu_usuario1_idx (usu_usuario_usu_id ASC),
  CONSTRAINT fk_tur_turma_usu_usuario1
    FOREIGN KEY (usu_usuario_usu_id)
    REFERENCES nota.usu_usuario (usu_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




CREATE TABLE ava_avaliacao (
  ava_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  ava_nota DOUBLE,
  ava_descricao TEXT,
  ava_id_aluno INT,
  PRIMARY KEY (ava_id))
ENGINE = InnoDB;

CREATE TABLE cur_cursos (
  cur_id INT NOT NULL AUTO_INCREMENT UNIQUE,
  cur_nome VARCHAR(45),
  cur_descricao VARCHAR(45),
  cur_faixaetaria VARCHAR(45),
  tur_turma_tur_id INT,
  ava_avaliacao_ava_id INT,
  PRIMARY KEY (cur_id),
  INDEX fk_cur_cursos_tur_turma1_idx (tur_turma_tur_id ASC),
  INDEX fk_cur_cursos_ava_avaliacao1_idx (ava_avaliacao_ava_id ASC),
  CONSTRAINT fk_cur_cursos_tur_turma1
    FOREIGN KEY (tur_turma_tur_id)
    REFERENCES nota.tur_turma (tur_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_cur_cursos_ava_avaliacao1
    FOREIGN KEY (ava_avaliacao_ava_id)
    REFERENCES nota.ava_avaliacao (ava_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE tdc_tipo_contato (
  tdc_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  tdc_descricao TEXT,
  PRIMARY KEY (tdc_id))
ENGINE = InnoDB;


CREATE TABLE con_contato (
  con_id INT NOT NULL AUTO_INCREMENT UNIQUE,
  con_valor VARCHAR(45),
  tdc_id INT,
  usu_id INT,
  PRIMARY KEY (con_id),
  INDEX fk_con_contato_tdc_tipo_contato1_idx (tdc_id ASC),
  INDEX fk_con_contato_usu_usuario1_idx (usu_id ASC),
  CONSTRAINT fk_con_contato_tdc_tipo_contato1
    FOREIGN KEY (tdc_id)
    REFERENCES nota.tdc_tipo_contato (tdc_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_con_contato_usu_usuario1
    FOREIGN KEY (usu_id)
    REFERENCES nota.usu_usuario (usu_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



CREATE TABLE mod_modulos (
  mod_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  mod_aula VARCHAR(45),
  mod_teste VARCHAR(45),
  mod_jogo VARCHAR(45),
  mod_avaliação_final VARCHAR(45),
  cur_cursos_cur_id INT,
  PRIMARY KEY (mod_id),
  INDEX fk_mod_modulos_cur_cursos1_idx (cur_cursos_cur_id ASC),
  CONSTRAINT fk_mod_modulos_cur_cursos1
    FOREIGN KEY (cur_cursos_cur_id)
    REFERENCES nota.cur_cursos (cur_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



CREATE TABLE aul_aula (
  aul_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  aul_pagina VARCHAR(45),
  aul_textos VARCHAR(45),
  aul_imagens VARCHAR(45),
  aul_videos VARCHAR(45),
  aul_audios VARCHAR(45),
  mod_modulos_mod_id INT,
  PRIMARY KEY (aul_id),
  INDEX fk_aul_aula_mod_modulos1_idx (mod_modulos_mod_id ASC),
  CONSTRAINT fk_aul_aula_mod_modulos1
    FOREIGN KEY (mod_modulos_mod_id)
    REFERENCES nota.mod_modulos (mod_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE tes_testes (
  tes_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  tes_nota VARCHAR(45),
  tes_numeracao VARCHAR(45),
  aul_aula_aul_id INT,
  PRIMARY KEY (tes_id),
  INDEX fk_tes_testes_aul_aula1_idx (aul_aula_aul_id ASC),
  CONSTRAINT fk_tes_testes_aul_aula1
    FOREIGN KEY (aul_aula_aul_id)
    REFERENCES nota.aul_aula (aul_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE qet_questao (
  qet_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  qet_nome VARCHAR(120),
  qet_descricao TEXT,
  qet_resposta VARCHAR(1),
  qet_valor INT,
  tes_testes_tes_id INT,
  ava_avaliacao_ava_id INT,
  PRIMARY KEY (qet_id, ava_avaliacao_ava_id),
  INDEX fk_qet_questao_tes_testes1_idx (tes_testes_tes_id ASC),
  INDEX fk_qet_questao_ava_avaliacao1_idx (ava_avaliacao_ava_id ASC),
  CONSTRAINT fk_qet_questao_tes_testes1
    FOREIGN KEY (tes_testes_tes_id)
    REFERENCES nota.tes_testes (tes_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_qet_questao_ava_avaliacao1
    FOREIGN KEY (ava_avaliacao_ava_id)
    REFERENCES nota.ava_avaliacao (ava_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE cro_cronograma (
  cro_id INT NOT NULL UNIQUE AUTO_INCREMENT,
  cro_data DATETIME,
  cro_tipo INT,
  cro_descricao TEXT,
  usu_usuario_usu_id INT,
  PRIMARY KEY (cro_id),
  INDEX fk_cro_cronograma_usu_usuario1_idx (usu_usuario_usu_id ASC),
  CONSTRAINT fk_cro_cronograma_usu_usuario1
    FOREIGN KEY (usu_usuario_usu_id)
    REFERENCES nota.usu_usuario (usu_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

