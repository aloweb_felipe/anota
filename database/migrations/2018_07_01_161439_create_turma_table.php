<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurmaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

//        Schema::create('turma', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('name');
//            $table->string('periodo');
//
//            $table->unsignedInteger('escola');
//
//            $table->foreign('escola')->references('id')->on('escola');
//
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turma');
    }
}
