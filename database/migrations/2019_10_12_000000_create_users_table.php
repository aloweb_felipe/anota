<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        ini_set('memory_limit', '-1');
//        DB::connection()->getPdo()->exec(file_get_contents('/home/paulo/Área de Trabalho/anota/db/nota.sql'));

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');

            $table->string('imagem');
            $table->date('dataNascimento');
            $table->char('sexo');
            $table->string('documento');
            $table->string('ra');
            $table->tinyInteger('visibilidade');
            $table->unsignedInteger('tipo');
//            $table->unsignedInteger('turma');

            $table->foreign('tipo')->references('id')->on('tipo_users');
//            $table->foreign('turma')->references('id')->on('turma');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
