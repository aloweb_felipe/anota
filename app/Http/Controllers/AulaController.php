<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AulaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dialogo_anotacao = "Fazer fazer a aula 1 ???";
        return view('aula', compact('dialogo_anotacao'));
    }

    public function aula1(){
        $dialogo_anotacao = "Você pode pedir ajuda a qualquer momento...";
        return view('aulas.aula1', compact('dialogo_anotacao'));
    }


}

