<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class PerfilController extends Controller
{
    public function index(){
        return view('perfil');
    }

    public function edit(){
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return view('perfil', compact('dialogo_anotacao'));
    }

    public function avatar(){
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return view('avatar', compact('dialogo_anotacao'));
    }

    public function notas(){
        $dialogo_anotacao = "Estas são as notas do ".auth::user()->name;
     return view('notas', compact('dialogo_anotacao'));
    }

    public function aranha(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/aranha.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }

    public function batman(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/batman.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }

    public function capitao(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/capitao.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }

    public function ciborgue(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/ciborgue.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }

    public function deadpool(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/deadpool.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }

    public function estranho(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/estranho.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }

    public function fenix(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/fenix.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }

    public function ferro(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/ferro.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
    public function flash(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/flash.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
    public function hulk(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/hulk.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
    public function lanterna(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/lanterna.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
    public function mulher(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/mulher.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
    public function storm(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/storm.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
    public function superman(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/superman.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
    public function thor(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/thor.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
    public function wanda(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/wanda.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
    public function wolverine(){
        $user = User::findOrFail(auth::user()->id);
        $user->imagem = "/img/avatar/wolverine.png";
        $user->save();
        $dialogo_anotacao = "Clique no avatar que deseja, ".auth::user()->name;
        return redirect()->route('avatar', compact('dialogo_anotacao'));
    }
}
