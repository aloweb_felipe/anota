<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SuporteController extends Controller
{
    public function index(){
        $dialogo_anotacao = "Qual a sua duvida?";
        return view('suporte', compact('dialogo_anotacao'));
    }
}
