<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('aula', 'AulaController@index')->name('aula');
Route::get('perfil', 'PerfilController@index')->name('perfil');
Route::get('notas', 'PerfilController@notas')->name('notas');
Route::get('avatar', 'PerfilController@avatar')->name('avatar');
Route::get('avaliacao', 'AvaliacaoController@index')->name('avaliacao');
Route::get('provas', 'ProvasController@index')->name('provas');
Route::get('suporte', 'SuporteController@index')->name('suporte');


Route::get('aula/1', 'AulaController@aula1')->name('aula1');




Route::get('aranha', 'PerfilController@aranha')->name('aranha');
Route::get('batman', 'PerfilController@batman')->name('batman');
Route::get('capitao', 'PerfilController@capitao')->name('capitao');
Route::get('ciborgue', 'PerfilController@ciborgue')->name('ciborgue');
Route::get('deadpool', 'PerfilController@deadpool')->name('deadpool');
Route::get('estranho', 'PerfilController@estranho')->name('estranho');
Route::get('fenix', 'PerfilController@fenix')->name('fenix');
Route::get('ferro', 'PerfilController@ferro')->name('ferro');
Route::get('flash', 'PerfilController@flash')->name('flash');
Route::get('hulk', 'PerfilController@hulk')->name('hulk');
Route::get('lanterna', 'PerfilController@lanterna')->name('lanterna');
Route::get('mulher', 'PerfilController@mulher')->name('mulher');
Route::get('storm', 'PerfilController@storm')->name('storm');
Route::get('superman', 'PerfilController@superman')->name('superman');
Route::get('thor', 'PerfilController@thor')->name('thor');
Route::get('wanda', 'PerfilController@wanda')->name('wanda');
Route::get('wolverine', 'PerfilController@wolverine')->name('wolverine');


Route::get('/home', 'HomeController@index')->name('home');
